# Internship Assignments

Assignments from Tangible A.I. Internship

First Assignment- Fake News Detection using Naive Bayes and Logistic Regression models

Internship Project files:<br>
<b>BookSum_Preprocessing_FeatEng.ipynb</b>- preprocessing and feature engineering of dataset

<b>BookSum_EDA.ipynb</b>- exploratory data analysis

<b>BoomSum_Modeling</b>- machine learning models and experiments

<b>MultilabelGenreClassReport.ipynb</b>- final report
